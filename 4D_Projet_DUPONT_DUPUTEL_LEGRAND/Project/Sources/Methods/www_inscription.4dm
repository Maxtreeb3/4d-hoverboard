//%attributes = {"publishedWeb":true}
C_TEXT:C284($0;$1)

C_TEXT:C284($prenom;$nom;$email;$password;$nfc)
C_LONGINT:C283($curseur)
C_OBJECT:C1216($e_new_USR)


ARRAY TEXT:C222($at_names;0)
ARRAY TEXT:C222($at_values;0)
WEB GET VARIABLES:C683($at_names;$at_values)

$curseur:=Find in array:C230($at_names;"web_prenom")
If ($curseur>0)
	$prenom:=$at_values{$curseur}
End if 

$curseur:=Find in array:C230($at_names;"web_nom")
If ($curseur>0)
	$nom:=$at_values{$curseur}
End if 

$curseur:=Find in array:C230($at_names;"web_email")
If ($curseur>0)
	$email:=$at_values{$curseur}
End if 

$curseur:=Find in array:C230($at_names;"web_password")
If ($curseur>0)
	$password:=$at_values{$curseur}
End if 

$curseur:=Find in array:C230($at_names;"web_nfc")
If ($curseur>0)
	$nfc:=$at_values{$curseur}
End if 

$e_new_USR:=ds:C1482.UTILISATEUR.new()
$e_new_USR.Prenom:=$prenom
$e_new_USR.Nom:=$nom
$e_new_USR.Email:=$email
$e_new_USR.NFC:=$nfc
$e_new_USR.Password_hash:=Generate digest:C1147($password;SHA256 digest:K66:4)
$e_new_USR.Location:="false"
$e_new_USR.save()

WEB SEND HTTP REDIRECT:C659("/login.shtml")